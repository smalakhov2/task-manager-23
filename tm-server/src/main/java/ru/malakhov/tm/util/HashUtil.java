package ru.malakhov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class HashUtil {

    @NotNull
    private static final String SECRET = "36573658";

    @NotNull
    private static final Integer ITERATION = 198;

    @Nullable
    public static String salt(@NotNull final String value) {
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            if (result != null) result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @Nullable
    public static String md5(@NotNull final String value) {
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            final StringBuilder stringBuffer = new StringBuilder();
            for (byte b : array) {
                stringBuffer.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}