package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    public Session() {
    }

    public Session(
            @Nullable final Long timestamp,
            @Nullable final String userId,
            @Nullable final String signature
    ) {
        this.timestamp = timestamp;
        this.userId = userId;
        this.signature = signature;
    }

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Session && super.equals(o)) {
            @NotNull final Session session = (Session) o;
            return Objects.equals(timestamp, session.timestamp)
                    && Objects.equals(userId, session.userId)
                    && Objects.equals(signature, session.signature);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, userId, signature);
    }

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NotNull Long timestamp) {
        this.timestamp = timestamp;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setSignature(@NotNull String signature) {
        this.signature = signature;
    }

}