package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain implements Serializable {

    public static final long serialVersionUID = 1;

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

    @NotNull
    private List<Session> sessions = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Domain domain = (Domain) o;
        return projects.equals(domain.projects)
                && tasks.equals(domain.tasks)
                && users.equals(domain.users)
                && sessions.equals(domain.sessions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projects, tasks, users, sessions);
    }

    @NotNull
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    @NotNull
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }

    @NotNull
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

    @NotNull
    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(@NotNull List<Session> sessions) {
        this.sessions = sessions;
    }

}