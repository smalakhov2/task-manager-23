package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IAdminUserEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Fail;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.dto.Success;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getUserService().clear();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> getAllUserList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByLogin(login);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByLogin(login);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByLogin(login);
        return serviceLocator.getUserService().removeByLogin(login);
    }

}
