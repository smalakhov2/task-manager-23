package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IProjectEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Fail;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.dto.Success;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllProject(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getProjectService().clear();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> getAllProjectList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public Result createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().create(session.getUserId(), name, description);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearProjectList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().clear(session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> getProjectList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project getProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project getProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project getProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectById(
                session.getUserId(),
                id,
                name,
                description
        );
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectByIndex(
                session.getUserId(),
                index,
                name,
                description
        );
    }

}
