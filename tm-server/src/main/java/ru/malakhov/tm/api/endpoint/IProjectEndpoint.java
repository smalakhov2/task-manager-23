package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllProject(
            @WebParam(name = "session") @Nullable final Session session
    );

    @NotNull
    @WebMethod
    List<Project> getAllProjectList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result createProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    Result clearProjectList(
            @WebParam(name = "session") @Nullable Session session
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<Project> getProjectList(
            @WebParam(name = "session") @Nullable Session session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Project removeProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Project removeProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Project removeProjectByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Project getProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @NotNull
    @WebMethod
    Project getProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Project getProjectByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Project updateProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Project updateProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;

}
