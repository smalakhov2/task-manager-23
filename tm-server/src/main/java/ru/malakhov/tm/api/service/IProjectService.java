package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name) throws AbstractException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException;

    void add(@Nullable String userId, @Nullable Project project) throws AbstractException;

    void remove(@Nullable String userId, @Nullable Project project) throws AbstractException;

    @NotNull
    List<Project> findAll(@Nullable String userId) throws EmptyUserIdException;

    void clear(@Nullable String userId) throws EmptyUserIdException;

    @NotNull
    Project findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    Project findOneByIndex(@Nullable String userId, int index) throws AbstractException;

    @NotNull
    Project findOneByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    @Nullable
    Project updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @Nullable
    Project updateProjectByIndex(
            @Nullable String userId,
            int index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    Project removeOneByIndex(@Nullable String userId, int index) throws AbstractException;

    @Nullable
    Project removeOneByName(@Nullable String userId, @Nullable String name) throws AbstractException;

}
