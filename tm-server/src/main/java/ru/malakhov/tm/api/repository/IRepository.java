package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void clear();

    void add(@NotNull E entity);

    void add(@NotNull Collection<E> entities);

    void add(@NotNull E... entities);

    @Nullable
    E remove(@NotNull E entity);

    @Nullable
    E removeById(@NotNull String id);

    @Nullable
    E findById(@NotNull String id);

    void load(@NotNull Collection<E> entities);

    void load(@NotNull E... entities);

    boolean contains(@NotNull String id);

    boolean contains(@NotNull final E entity);

    int count();

}
