package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllTask(
            @WebParam(name = "session") @Nullable final Session session
    );

    @NotNull
    @WebMethod
    List<Task> getAllTaskList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result createTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    Result clearTaskList(@WebParam(name = "session") @Nullable Session session);

    @NotNull
    @WebMethod
    List<Task> getTaskList(
            @WebParam(name = "session") @Nullable Session session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Task removeTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Task removeTaskByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Task getTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @NotNull
    @WebMethod
    Task getTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Task getTaskByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;

}
