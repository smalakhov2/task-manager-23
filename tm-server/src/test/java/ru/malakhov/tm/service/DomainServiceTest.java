package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.Domain;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyDomainException;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class DomainServiceTest {

    @NotNull
    private final IServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IDomainService domainService = new DomainService(serviceLocator);

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("Uknown", HashUtil.salt("3333"), "3@3.ru");

    @NotNull
    private final List<User> allUserInSystem = new ArrayList<>(Arrays.asList(user1, user2));

    @NotNull
    private final Project project1 = new Project("Project1", "project1", user1.getId());

    @NotNull
    private final Project project2 = new Project("Project2", "project2", user2.getId());

    @NotNull
    final Project unknownProject = new Project("Unknown", "unknown", unknownUser.getId());

    @NotNull
    private final List<Project> allProjectInSystem = new ArrayList<>(Arrays.asList(project1, project2));

    @NotNull
    private final Task task1 = new Task("Task1", "task1", user1.getId());

    @NotNull
    private final Task task2 = new Task("Task2", "task2", user2.getId());

    @NotNull
    final Task unknownTask = new Task("Unknown", "unknown", unknownUser.getId());

    @NotNull
    private final List<Task> allTaskInSystem = new ArrayList<>(Arrays.asList(task1, task2));

    @NotNull
    private final Session sessionOne = new Session(System.currentTimeMillis(), user1.getId(), null);

    @NotNull
    private final Session sessionTwo = new Session(System.currentTimeMillis(), user2.getId(), null);

    @NotNull
    private final Session unknownSession = new Session(System.currentTimeMillis(), unknownUser.getId(), null);

    @NotNull
    private final List<Session> allSessionInSystem = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    private void loadData() {
        serviceLocator.getProjectService().add(allProjectInSystem);
        serviceLocator.getUserService().add(allUserInSystem);
        serviceLocator.getTaskService().add(allTaskInSystem);
        serviceLocator.getSessionService().add(allSessionInSystem);
    }

    @After
    public void after() {
        serviceLocator.getUserService().clear();
        serviceLocator.getSessionService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
    }

    @Test
    public void testSaveData() throws EmptyDomainException {
        loadData();
        @NotNull final Domain domain = new Domain();

        domainService.saveData(domain);

        @NotNull final List<Project> projects = domain.getProjects();
        Assert.assertNotEquals(0, projects.size());
        Assert.assertEquals(projects.hashCode(), allProjectInSystem.hashCode());
        Assert.assertEquals(projects, allProjectInSystem);

        @NotNull final List<Task> tasks = domain.getTasks();
        Assert.assertNotEquals(0, tasks.size());
        Assert.assertEquals(tasks.hashCode(), allTaskInSystem.hashCode());
        Assert.assertEquals(tasks, allTaskInSystem);

        @NotNull final List<Session> sessions = domain.getSessions();
        Assert.assertNotEquals(0, sessions.size());
        Assert.assertEquals(sessions.hashCode(), allSessionInSystem.hashCode());
        Assert.assertEquals(sessions, allSessionInSystem);

        @NotNull final List<User> users = domain.getUsers();
        Assert.assertNotEquals(0, users.size());
        Assert.assertEquals(users.hashCode(), allUserInSystem.hashCode());
        Assert.assertEquals(users, allUserInSystem);
    }

    @Test(expected = EmptyDomainException.class)
    public void testSaveDataWithoutDomain() throws EmptyDomainException {
        domainService.saveData(null);
    }

    @Test
    public void testLoadData() throws AbstractException {
        serviceLocator.getProjectService().add(unknownProject);
        serviceLocator.getTaskService().add(unknownTask);
        serviceLocator.getUserService().add(unknownUser);
        serviceLocator.getSessionService().add(unknownSession);
        @NotNull final Domain domain = new Domain();
        domain.setProjects(allProjectInSystem);
        domain.setTasks(allTaskInSystem);
        domain.setUsers(allUserInSystem);
        domain.setSessions(allSessionInSystem);

        domainService.loadData(domain);
        @NotNull final List<Project> projectsInSystem = serviceLocator.getProjectService().findAll();
        Assert.assertNotEquals(0, projectsInSystem.size());
        Assert.assertEquals(projectsInSystem.hashCode(), allProjectInSystem.hashCode());
        Assert.assertEquals(projectsInSystem, allProjectInSystem);
        @Nullable final Project unknProject = serviceLocator.getProjectService().findById(unknownProject.getId());
        Assert.assertNull(unknProject);

        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertNotEquals(0, tasks.size());
        Assert.assertEquals(tasks.hashCode(), allTaskInSystem.hashCode());
        Assert.assertEquals(tasks, allTaskInSystem);
        @Nullable final Task unknTask = serviceLocator.getTaskService().findById(unknownTask.getId());
        Assert.assertNull(unknTask);

        @NotNull final List<Session> sessions = serviceLocator.getSessionService().findAll();
        Assert.assertNotEquals(0, sessions.size());
        Assert.assertEquals(sessions.hashCode(), allSessionInSystem.hashCode());
        Assert.assertEquals(sessions, allSessionInSystem);
        @Nullable final Session unknSession = serviceLocator.getSessionService().findById(unknownSession.getId());
        Assert.assertNull(unknSession);

        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertNotEquals(0, users.size());
        Assert.assertEquals(users.hashCode(), allUserInSystem.hashCode());
        Assert.assertEquals(users, allUserInSystem);
        @Nullable final User unknUser = serviceLocator.getUserService().findById(unknownUser.getId());
        Assert.assertNull(unknUser);
    }

    @Test(expected = EmptyDomainException.class)
    public void testLoadDataWithoutDomain() throws EmptyDomainException {
        domainService.loadData(null);
    }

}