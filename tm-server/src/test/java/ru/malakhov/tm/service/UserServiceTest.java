package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.util.HashUtil;

@Category(DataCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("UnknownUser", HashUtil.salt("3333"), "3@3.ru");

    private void loadData() {
        userService.add(user1, user2);
    }

    @After
    public void after() {
        userService.clear();
    }

    @Test
    public void testCreateEmptyService() {
        Assert.assertEquals(0, userService.count());
    }

    @Test
    public void testFindByLogin() throws AbstractException {
        loadData();

        @NotNull final User user = userService.findByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user1.hashCode(), user.hashCode());
        Assert.assertEquals(user1, user);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindByLoginWithoutLogin() throws AbstractException {
        userService.findByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testFindByLoginWithEmptyLogin() throws AbstractException {
        userService.findByLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testFindByLoginWithUnknownLogin() throws AbstractException {
        userService.findByLogin(unknownUser.getLogin());
    }

    @Test
    public void testRemoveByLogin() throws AbstractException {
        loadData();

        final int counter = userService.count();

        @Nullable User user = userService.removeByLogin(unknownUser.getLogin());
        Assert.assertNull(user);
        Assert.assertEquals(counter, userService.count());

        user = userService.removeByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user1, user);
        Assert.assertEquals(counter - 1, userService.count());
        user = userService.findById(user1.getId());
        Assert.assertNull(user);
    }

    @Test(expected = EmptyLoginException.class)
    public void testRemoveByLoginWithoutLogin() throws AbstractException {
        userService.removeByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testRemoveByLoginWithEmptyLogin() throws AbstractException {
        userService.removeByLogin("");
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final User user = userService.create(
                user1.getLogin(),
                "1111"
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user.getPasswordHash(), user1.getPasswordHash());
        Assert.assertEquals(Role.USER, user.getRole());
        @Nullable final User userInStorage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorage);
        Assert.assertEquals(user, userInStorage);
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithoutPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                null
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmptyPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                ""
        );
    }

    @Test
    public void testCreateWithEmail() throws AbstractException {
        @NotNull final User user = userService.create(
                user1.getLogin(),
                "1111",
                user1.getEmail()
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user.getPasswordHash(), user1.getPasswordHash());
        Assert.assertEquals(user.getEmail(), user1.getEmail());
        Assert.assertEquals(user.getRole(), Role.USER);
        @Nullable final User userInStorage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorage);
        Assert.assertEquals(user, userInStorage);
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmailWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111",
                user1.getEmail()
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmailWithoutPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                null,
                user1.getEmail()
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testCreateWithEmailWithoutEmail() throws AbstractException {
        @Nullable final String email = null;
        userService.create(
                user1.getLogin(),
                "1111",
                email
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmailEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111",
                user1.getEmail()
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmailEmptyPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                "",
                user1.getEmail()
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testCreateWithEmailEmptyEmail() throws AbstractException {
        userService.create(
                user1.getLogin(),
                "1111",
                ""
        );
    }

    @Test
    public void testCreateWithRole() throws AbstractException {
        @NotNull User user = userService.create(
                user1.getLogin(),
                "1111",
                Role.USER
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user.getPasswordHash(), user1.getPasswordHash());
        Assert.assertEquals(user.getRole(), Role.USER);
        @Nullable User userInStorage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorage);
        Assert.assertEquals(user, userInStorage);
        Assert.assertEquals(user.getRole(), Role.USER);

        user = userService.create(
                user1.getLogin(),
                "1111",
                Role.ADMIN
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user.getPasswordHash(), user1.getPasswordHash());
        Assert.assertEquals(user.getRole(), Role.ADMIN);
        userInStorage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorage);
        Assert.assertEquals(user, userInStorage);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithRoleWithoutLogin() throws AbstractException {
        userService.create(
                null,
                "1111",
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleWithoutPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                null,
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleWithoutRole() throws AbstractException {
        @Nullable final Role role = null;
        userService.create(
                user1.getLogin(),
                "",
                role
        );
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithRoleEmptyLogin() throws AbstractException {
        userService.create(
                "",
                "1111",
                Role.USER
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleEmptyPassword() throws AbstractException {
        userService.create(
                user1.getLogin(),
                "",
                Role.USER
        );
    }

    @Test
    public void testUpdatePassword() {

    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdatePasswordWithoutUserId() throws AbstractException {
        userService.updatePassword(
                null,
                "1111",
                "1112"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testUpdatePasswordWithoutPassword() throws AbstractException {
        userService.updatePassword(
                user1.getId(),
                null,
                "1112"
        );
    }

    @Test(expected = EmptyNewPasswordException.class)
    public void testUpdatePasswordWithoutNewPassword() throws AbstractException {
        userService.updatePassword(
                user1.getId(),
                "1111",
                null
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdatePasswordWithEmptyUserId() throws AbstractException {
        userService.updatePassword(
                "",
                "1111",
                "1112"
        );
    }

    @Test(expected = EmptyPasswordException.class)
    public void testUpdatePasswordWithEmptyPassword() throws AbstractException {
        userService.updatePassword(
                user1.getId(),
                "",
                "1112"
        );
    }

    @Test(expected = EmptyNewPasswordException.class)
    public void testUpdatePasswordWithEmptyNewPassword() throws AbstractException {
        userService.updatePassword(
                user1.getId(),
                "1111",
                ""
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdatePasswordWithUnknownUserId() throws AbstractException {
        userService.updatePassword(
                unknownUser.getId(),
                "1111",
                "1112"
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdatePasswordWithIncorrectCurrentPassword() throws AbstractException {
        userService.updatePassword(
                user1.getId(),
                "1113",
                "1112"
        );
    }

    @Test
    public void testUpdateUserInfo() throws AbstractException {
        loadData();
        @NotNull final String newEmail = "yandex@yandex.ru";
        @NotNull final String newFirstName = "Ivan";
        @NotNull final String newLastName = "Ivanov";
        @NotNull final String newMiddleName = "Ivanovich";

        @NotNull final User user = userService.updateUserInfo(
                user1.getId(),
                newEmail,
                newFirstName,
                newLastName,
                newMiddleName
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(newEmail, user.getEmail());
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
        @Nullable final User userInStorage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorage);
        Assert.assertEquals(user.hashCode(), userInStorage.hashCode());
        Assert.assertEquals(user, userInStorage);
        Assert.assertEquals(userInStorage.getEmail(), user.getEmail());
        Assert.assertEquals(userInStorage.getFirstName(), user.getFirstName());
        Assert.assertEquals(userInStorage.getLastName(), user.getLastName());
        Assert.assertEquals(userInStorage.getMiddleName(), user.getMiddleName());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateUserInfoWithouUserId() throws AbstractException {
        userService.updateUserInfo(
                null,
                user1.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testUpdateUserInfoWithoutEmail() throws AbstractException {
        userService.updateUserInfo(
                user1.getId(),
                null,
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateUserInfoWithEmptyUserId() throws AbstractException {
        userService.updateUserInfo(
                "",
                user1.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = EmptyEmailException.class)
    public void testUpdateUserInfoWithEmptyEmail() throws AbstractException {
        userService.updateUserInfo(
                user1.getId(),
                "",
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateUserInfoWithUnknownUserId() throws AbstractException {
        userService.updateUserInfo(
                unknownUser.getId(),
                user1.getEmail(),
                "Ivan",
                "Ivanov",
                "Ivanovich"
        );
    }

    @Test
    public void testLockUserByLogin() throws AbstractException {
        loadData();

        @NotNull final User user = userService.lockUserByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getLocked());
        @Nullable final User userInStorrage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorrage);
        Assert.assertEquals(user.hashCode(), userInStorrage.hashCode());
        Assert.assertEquals(user, userInStorrage);
        Assert.assertTrue(userInStorrage.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void testLockUserByLoginWithoutLogin() throws AbstractException {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testLockUserByLoginWithEmptyLogin() throws AbstractException {
        userService.lockUserByLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testLockUserByLoginWithUnknownLogin() throws AbstractException {
        userService.lockUserByLogin(unknownUser.getLogin());
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException {
        loadData();

        @NotNull final User user = userService.unlockUserByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
        @Nullable final User userInStorrage = userService.findById(user.getId());
        Assert.assertNotNull(userInStorrage);
        Assert.assertEquals(user.hashCode(), userInStorrage.hashCode());
        Assert.assertEquals(user, userInStorrage);
        Assert.assertFalse(userInStorrage.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void testUnlockUserByLoginWithoutLogin() throws AbstractException {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testUnlockUserByLoginWithEmptyLogin() throws AbstractException {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testUnlockUserByLoginWithUnknownLogin() throws AbstractException {
        userService.unlockUserByLogin(unknownUser.getLogin());
    }

}
