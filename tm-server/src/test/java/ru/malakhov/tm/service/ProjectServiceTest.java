package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyProjectException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.exception.unknown.UnknownProjectException;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("User3", HashUtil.salt("2222"), "3@3.ru");

    @NotNull
    private final Project project1 = new Project("Project1", null, user1.getId());

    @NotNull
    private final Project project2 = new Project("Project2", null, user1.getId());

    @NotNull
    private final Project project3 = new Project("Project3", null, user2.getId());

    @NotNull
    private final Project project4 = new Project("Project4", null, user2.getId());

    @NotNull
    final Project unknownProject = new Project("Unknown", null, unknownUser.getId());

    @NotNull
    private final List<Project> user1Projects = new ArrayList<>(Arrays.asList(project1, project2));

    @NotNull
    private final List<Project> user2Projects = new ArrayList<>(Arrays.asList(project3, project4));

    private void loadData() {
        projectService.add(user1Projects);
        projectService.add(user2Projects);
    }

    @After
    public void after() {
        projectService.clear();
    }

    @Test
    public void testCreateEmptyService() {
        Assert.assertEquals(0, projectService.count());
    }

    @Test
    public void testCreate() throws AbstractException {
        final String userId = project1.getUserId();
        @NotNull final String projectName = project1.getName();

        projectService.create(userId, projectName);
        @NotNull final Project project = projectService.findOneByName(userId, projectName);
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithoutUserId() throws AbstractException {
        projectService.create(null, project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithoutName() throws AbstractException {
        projectService.create(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithEmptyUserId() throws AbstractException {
        projectService.create("", project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithEmptyName() throws AbstractException {
        projectService.create(project1.getUserId(), "");
    }

    @Test
    public void testCreateWithDescription() throws AbstractException {
        final String userId = project1.getUserId();
        @NotNull final String projectName = project1.getName();
        @Nullable final String projectDescription = project1.getDescription();

        projectService.create(userId, projectName, projectDescription);
        @NotNull final Project project = projectService.findOneByName(userId, projectName);
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionWithoutUserId() throws AbstractException {
        projectService.create(null, project1.getName(), project1.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionWithoutName() throws AbstractException {
        projectService.create(project1.getUserId(), null, project1.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionEmptyUserId() throws AbstractException {
        projectService.create("", project1.getName(), project1.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionEmptyName() throws AbstractException {
        projectService.create(project1.getUserId(), "", project1.getDescription());
    }

    @Test
    public void testAdd() throws AbstractException {
        projectService.add(project1.getUserId(), project1);
        Assert.assertEquals(1, projectService.count());
        @Nullable final Project project = projectService.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testAddWithoutUserId() throws AbstractException {
        projectService.add(null, project1);
    }

    @Test(expected = EmptyProjectException.class)
    public void testAddWithoutProject() throws AbstractException {
        projectService.add(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testAddWithEmptyUserId() throws AbstractException {
        projectService.add("", project1);
    }

    @Test
    public void testRemove() throws AbstractException {
        loadData();
        int counter = projectService.count();

        projectService.remove(unknownProject.getUserId(), unknownProject);
        Assert.assertEquals(counter, projectService.count());

        projectService.remove(project1.getUserId(), project1);
        Assert.assertEquals(counter - 1, projectService.count());
        @Nullable final Project project = projectService.findById(project1.getId());
        Assert.assertNull(project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveWithoutUserId() throws AbstractException {
        projectService.remove(null, project1);
    }

    @Test(expected = EmptyProjectException.class)
    public void testRemoveWithoutProject() throws AbstractException {
        projectService.remove(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveWithEmptyUserId() throws AbstractException {
        projectService.remove("", project1);
    }

    @Test
    public void testFindAll() throws EmptyUserIdException {
        loadData();

        @NotNull List<Project> projects = projectService.findAll(unknownProject.getUserId());
        Assert.assertEquals(0, projects.size());

        projects = projectService.findAll(project1.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(user1Projects.hashCode(), projects.hashCode());
        Assert.assertEquals(user1Projects.size(), projects.size());
        Assert.assertEquals(user1Projects, projects);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllWithoutUserId() throws EmptyUserIdException {
        projectService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllWithEmptyUserId() throws EmptyUserIdException {
        projectService.findAll("");
    }

    @Test
    public void testClear() throws EmptyUserIdException {
        loadData();
        int counter = projectService.count();

        projectService.clear(unknownProject.getUserId());
        Assert.assertEquals(counter, projectService.count());

        projectService.clear(project1.getUserId());
        counter -= user1Projects.size();
        Assert.assertEquals(counter, projectService.count());
        for (@NotNull final Project project : user1Projects) {
            Assert.assertFalse(projectService.contains(project));
        }
    }

    @Test(expected = EmptyUserIdException.class)
    public void testClearWithoutUserId() throws EmptyUserIdException {
        projectService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testClearWithEmptyUserId() throws EmptyUserIdException {
        projectService.clear("");
    }

    @Test
    public void testFindById() throws AbstractException {
        loadData();
        final String userId = project1.getUserId();
        @NotNull final String id = project1.getId();

        @NotNull final Project project = projectService.findOneById(userId, id);
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIdWithoutUserId() throws AbstractException {
        loadData();
        projectService.findOneById(null, project1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdWithoutId() throws AbstractException {
        loadData();
        projectService.findOneById(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIdWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.findOneById("", project1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdWithEmptyId() throws AbstractException {
        loadData();
        projectService.findOneById(project1.getUserId(), "");
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByIdWithUnknownData() throws AbstractException {
        loadData();
        projectService.findOneById(unknownProject.getUserId(), unknownProject.getId());
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByIdWithUnknownUserId() throws AbstractException {
        loadData();
        projectService.findOneById(unknownProject.getUserId(), project1.getId());
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByIdWithUnknownId() throws AbstractException {
        loadData();
        projectService.findOneById(project1.getUserId(), unknownProject.getId());
    }

    @Test
    public void testFindByIndex() throws AbstractException {
        loadData();

        @Nullable final Project project = projectService.findOneByIndex(project1.getUserId(), 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIndexWithoutUserId() throws AbstractException {
        loadData();
        projectService.findOneByIndex(null, 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexWithIndexLessRange() throws AbstractException {
        projectService.findOneByIndex(project1.getUserId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        projectService.findOneByIndex(project1.getUserId(), 10);
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByIndexWithUnknownUserId() throws AbstractException {
        loadData();
        projectService.findOneByIndex(unknownProject.getUserId(), 0);
    }

    @Test
    public void testFindByName() throws AbstractException {
        loadData();
        final String userId = project1.getUserId();
        @NotNull final String name = project1.getName();

        @NotNull final Project project = projectService.findOneByName(userId, name);
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByNameWithoutUserId() throws AbstractException {
        loadData();
        projectService.findOneByName(null, project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameWithoutName() throws AbstractException {
        loadData();
        projectService.findOneByName(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByNameWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.findOneByName("", project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameWithEmptyName() throws AbstractException {
        loadData();
        projectService.findOneByName(project1.getUserId(), "");
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByNameWithUnknownData() throws AbstractException {
        loadData();
        projectService.findOneByName(unknownProject.getUserId(), unknownProject.getName());
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByNameWithUnknownUserId() throws AbstractException {
        loadData();
        projectService.findOneByName(unknownProject.getUserId(), project1.getName());
    }

    @Test(expected = UnknownProjectException.class)
    public void testFindByIdWithUnknownName() throws AbstractException {
        loadData();
        projectService.findOneByName(project1.getUserId(), unknownProject.getName());
    }

    @Test
    public void testRemoveById() throws AbstractException {
        loadData();
        final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownId = unknownProject.getId();
        final String userId = project1.getUserId();
        @NotNull final String id = project1.getId();
        final int counter = projectService.count();

        @Nullable Project project = projectService.removeOneById(unknownUserId, unknownId);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());
        project = projectService.removeOneById(unknownUserId, id);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());
        project = projectService.removeOneById(userId, unknownId);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());

        project = projectService.removeOneById(userId, id);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectService.count());
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
        project = projectService.findById(project1.getId());
        Assert.assertNull(project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIdWithoutUserId() throws AbstractException {
        loadData();
        projectService.removeOneById(null, project1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdWithoutId() throws AbstractException {
        loadData();
        projectService.removeOneById(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIdWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.removeOneById("", project1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdWithEmptyId() throws AbstractException {
        loadData();
        projectService.removeOneById(project1.getUserId(), "");
    }

    @Test
    public void testRemoveByIndex() throws AbstractException {
        loadData();
        final String unknownUserId = unknownProject.getUserId();
        final String userId = project1.getUserId();
        final int index = 0;
        final int counter = projectService.count();

        @Nullable Project project = projectService.removeOneByIndex(unknownUserId, index);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());

        project = projectService.removeOneByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectService.count());
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
        project = projectService.findById(project1.getId());
        Assert.assertNull(project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIndexWithoutUserId() throws AbstractException {
        loadData();
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexWithIndexLessRange() throws AbstractException {
        projectService.removeOneByIndex(project1.getUserId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        projectService.removeOneByIndex(project1.getUserId(), 10);
    }

    @Test
    public void testRemoveByName() throws AbstractException {
        loadData();
        final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownName = unknownProject.getName();
        final String userId = project1.getUserId();
        @NotNull final String name = project1.getName();
        final int counter = projectService.count();

        @Nullable Project project = projectService.removeOneByName(unknownUserId, unknownName);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());
        project = projectService.removeOneByName(unknownUserId, name);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());
        project = projectService.removeOneByName(userId, unknownName);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectService.count());

        project = projectService.removeOneByName(userId, name);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectService.count());
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
        project = projectService.findById(project1.getId());
        Assert.assertNull(project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByNameWithoutUserId() throws AbstractException {
        loadData();
        projectService.removeOneByName(null, project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameWithoutName() throws AbstractException {
        loadData();
        projectService.removeOneByName(project1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByNameWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.removeOneByName("", project1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameWithEmptyName() throws AbstractException {
        loadData();
        projectService.removeOneByName(project1.getUserId(), "");
    }

    @Test
    public void testUpdateById() throws AbstractException {
        loadData();
        final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownId = unknownProject.getId();
        final String userId = project1.getUserId();
        @NotNull final String id = project1.getId();
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";

        @Nullable Project project = projectService.updateProjectById(
                unknownUserId,
                unknownId,
                newName,
                newDescription
        );
        Assert.assertNull(project);
        project = projectService.updateProjectById(
                unknownUserId,
                id,
                newName,
                newDescription
        );
        Assert.assertNull(project);
        project = projectService.updateProjectById(
                userId,
                unknownId,
                newName,
                newDescription
        );
        Assert.assertNull(project);

        project = projectService.updateProjectById(
                userId,
                id,
                newName,
                newDescription
        );
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
        project = projectService.findById(project.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithoutUserId() throws AbstractException {
        loadData();
        projectService.updateProjectById(
                null,
                project1.getId(),
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithoutId() throws AbstractException {
        loadData();
        projectService.updateProjectById(
                project1.getUserId(),
                null,
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithoutName() throws AbstractException {
        loadData();
        projectService.updateProjectById(
                project1.getUserId(),
                project1.getId(),
                null,
                project1.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.updateProjectById(
                "",
                project1.getId(),
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithEmptyId() throws AbstractException {
        loadData();
        loadData();
        projectService.updateProjectById(
                project1.getUserId(),
                "",
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithEmptyName() throws AbstractException {
        loadData();
        projectService.updateProjectById(
                project1.getUserId(),
                project1.getId(),
                "",
                project1.getDescription()
        );
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        loadData();
        final String unknownUserId = unknownProject.getUserId();
        final String userId = project1.getUserId();
        final int index = 0;
        @NotNull final String newName = "New name1";
        @NotNull final String newDescription = "New description1";

        @Nullable Project project = projectService.updateProjectByIndex(
                unknownUserId,
                index,
                newName,
                newDescription
        );
        Assert.assertNull(project);

        project = projectService.updateProjectByIndex(
                userId,
                index,
                newName,
                newDescription
        );
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
        project = projectService.findOneByIndex(project.getUserId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithoutUserId() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                null,
                0,
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithoutName() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                project1.getUserId(),
                0,
                null,
                project1.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                "",
                0,
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithEmptyName() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                project1.getUserId(),
                0,
                "",
                project1.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexLessRange() throws AbstractException {
        projectService.updateProjectByIndex(
                project1.getUserId(),
                -1,
                project1.getName(),
                project1.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                project1.getUserId(),
                10,
                project1.getName(),
                project1.getDescription()
        );
    }

}