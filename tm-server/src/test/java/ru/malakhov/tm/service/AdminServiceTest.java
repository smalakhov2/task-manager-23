package ru.malakhov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.service.IAdminService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.Domain;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.EmptyDomainException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.util.HashUtil;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AdminServiceTest {

    @NotNull
    private final IServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IAdminService adminService = new AdminService(serviceLocator);

    @NotNull
    private final String FILE_BINARY_PATH = "./data.binary";

    @NotNull
    private final String FILE_BASE64_PATH = "./data.base64";

    @NotNull
    private final String FILE_JSON_PATH = "./data.json";

    @NotNull
    private final String FILE_XML_PATH = "./data.xml";

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("Unknown", HashUtil.salt("3333"), "3@3.ru");

    @NotNull
    private final List<User> allUserInSystem = new ArrayList<>(Arrays.asList(user1, user2));

    @NotNull
    private final Project project1 = new Project("Project1", "project1", user1.getId());

    @NotNull
    private final Project project2 = new Project("Project2", "project2", user2.getId());

    @NotNull
    final Project unknownProject = new Project("Unknown", "unknown", unknownUser.getId());

    @NotNull
    private final List<Project> allProjectInSystem = new ArrayList<>(Arrays.asList(project1, project2));

    @NotNull
    private final Task task1 = new Task("Task1", "task1", user1.getId());

    @NotNull
    private final Task task2 = new Task("Task2", "task2", user2.getId());

    @NotNull
    final Task unknownTask = new Task("Unknown", "uknown", unknownUser.getId());

    @NotNull
    private final List<Task> allTaskInSystem = new ArrayList<>(Arrays.asList(task1, task2));

    @NotNull
    private final Session session1 = new Session(System.currentTimeMillis(), user1.getId(), null);

    @NotNull
    private final Session session2 = new Session(System.currentTimeMillis(), user2.getId(), null);

    @NotNull
    private final Session unknownSession = new Session(System.currentTimeMillis(), unknownUser.getId(), null);

    @NotNull
    private final List<Session> allSessionInSystem = new ArrayList<>(Arrays.asList(session1, session2));

    @NotNull
    private Domain loadData() throws EmptyDomainException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(allProjectInSystem);
        domain.setTasks(allTaskInSystem);
        domain.setUsers(allUserInSystem);
        domain.setSessions(allSessionInSystem);
        serviceLocator.getDomainService().loadData(domain);
        return domain;
    }

    private void loadDataUnknownData() {
        serviceLocator.getProjectService().add(unknownProject);
        serviceLocator.getTaskService().add(unknownTask);
        serviceLocator.getUserService().add(unknownUser);
        serviceLocator.getSessionService().add(unknownSession);
    }

    private void checkLoadedData() throws EmptyIdException {
        @NotNull final List<Project> projectsInSystem = serviceLocator.getProjectService().findAll();
        Assert.assertNotEquals(0, projectsInSystem.size());
        Assert.assertEquals(projectsInSystem.hashCode(), allProjectInSystem.hashCode());
        Assert.assertEquals(projectsInSystem, allProjectInSystem);
        @Nullable final Project unknProject = serviceLocator.getProjectService().findById(unknownProject.getId());
        Assert.assertNull(unknProject);

        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertNotEquals(0, tasks.size());
        Assert.assertEquals(tasks.hashCode(), allTaskInSystem.hashCode());
        Assert.assertEquals(tasks, allTaskInSystem);
        @Nullable final Task unknTask = serviceLocator.getTaskService().findById(unknownTask.getId());
        Assert.assertNull(unknTask);

        @NotNull final List<Session> sessions = serviceLocator.getSessionService().findAll();
        Assert.assertNotEquals(0, sessions.size());
        Assert.assertEquals(sessions.hashCode(), allSessionInSystem.hashCode());
        Assert.assertEquals(sessions, allSessionInSystem);
        @Nullable final Session unknSession = serviceLocator.getSessionService().findById(unknownSession.getId());
        Assert.assertNull(unknSession);

        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertNotEquals(0, users.size());
        Assert.assertEquals(users.hashCode(), allUserInSystem.hashCode());
        Assert.assertEquals(users, allUserInSystem);
        @Nullable final User unknUser = serviceLocator.getUserService().findById(unknownUser.getId());
        Assert.assertNull(unknUser);
    }

    public void clearApplicationData() {
        serviceLocator.getUserService().clear();
        serviceLocator.getSessionService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
    }

    @After
    public void after() throws IOException {
        clearApplicationData();
        @NotNull File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Test
    public void testSaveDataBinary() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataBinary();
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Assert.assertTrue(file.exists());
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final Domain domainInFile = (Domain) objectInputStream.readObject();
            Assert.assertEquals(domain.hashCode(), domainInFile.hashCode());
            Assert.assertEquals(domain, domainInFile);
        }
        adminService.clearDataBinary();
    }

    @Test
    public void testLoadDataBinary() throws Exception {
        loadData();
        adminService.saveDataBinary();
        clearApplicationData();
        loadDataUnknownData();

        adminService.loadDataBinary();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataBinaryWithoutDataFile() throws Exception {
        adminService.loadDataBinary();
    }

    @Test
    public void testClearDataBinary() throws IOException {
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataBinary();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataBase64() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataBase64();
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Assert.assertTrue(file.exists());
        final byte[] bytes64;
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BASE64_PATH)) {
            bytes64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        }
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes64);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final Domain domainInFile = (Domain) objectInputStream.readObject();
            Assert.assertEquals(domain.hashCode(), domainInFile.hashCode());
            Assert.assertEquals(domain, domainInFile);
        }
        adminService.clearDataBase64();
    }

    @Test
    public void testLoadDataBase64() throws Exception {
        loadData();
        adminService.saveDataBase64();
        clearApplicationData();
        loadDataUnknownData();

        adminService.loadDataBase64();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataBase64WithoutDataFile() throws Exception {
        adminService.loadDataBase64();
    }

    @Test
    public void testClearDataBase64() throws IOException {
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataBase64();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataJson() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataJson();
        @NotNull final File file = new File(FILE_JSON_PATH);
        Assert.assertTrue(file.exists());
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_JSON_PATH)) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domainInFile = objectMapper.readValue(fileInputStream, Domain.class);
            Assert.assertEquals(domain.hashCode(), domainInFile.hashCode());
            Assert.assertEquals(domain, domainInFile);
        }
        adminService.clearDataJson();
    }

    @Test
    public void testLoadDataJson() throws Exception {
        loadData();
        adminService.saveDataJson();
        clearApplicationData();
        loadDataUnknownData();

        adminService.loadDataJson();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataJsonWithoutDataFile() throws Exception {
        adminService.loadDataJson();
    }

    @Test
    public void testClearDataJson() throws IOException {
        @NotNull final File file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataJson();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataXml() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataXml();
        @NotNull final File file = new File(FILE_XML_PATH);
        Assert.assertTrue(file.exists());
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_XML_PATH)) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domainInFile = objectMapper.readValue(fileInputStream, Domain.class);
            Assert.assertEquals(domain.hashCode(), domainInFile.hashCode());
            Assert.assertEquals(domain, domainInFile);
        }
        adminService.clearDataXml();
    }

    @Test
    public void testLoadDataXml() throws Exception {
        loadData();
        adminService.saveDataXml();
        clearApplicationData();
        loadDataUnknownData();

        adminService.loadDataXml();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataXmlWithoutDataFile() throws Exception {
        adminService.loadDataXml();
    }

    @Test
    public void testClearDataXml() throws IOException {
        @NotNull final File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataXml();
        Assert.assertFalse(file.exists());
    }

}