package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final User userOne = new User(
            "User1",
            HashUtil.salt("1111"),
            "1@1.ru"
    );

    @NotNull
    private final User userTwo = new User(
            "User2",
            HashUtil.salt("2222"),
            "2@2.ru"
    );

    @NotNull
    private final User unknownUser = new User(
            "User2",
            HashUtil.salt("2222"),
            "2@2.ru"
    );

    @NotNull
    private final Session sessionOne = new Session(
            System.currentTimeMillis(),
            userOne.getId(),
            null
    );

    @NotNull
    private final Session sessionTwo = new Session(
            System.currentTimeMillis(),
            userOne.getId(),
            null
    );

    @NotNull
    private final Session sessionThree = new Session(
            System.currentTimeMillis(),
            userTwo.getId(),
            null
    );

    @NotNull
    private final Session sessionFour = new Session(
            System.currentTimeMillis(),
            userTwo.getId(),
            null
    );

    @NotNull
    private final Session unknownSession = new Session(
            System.currentTimeMillis(),
            unknownUser.getId(),
            null
    );

    @NotNull
    private final List<Session> userOneSessions = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    @NotNull
    private final List<Session> userTwoSessions = new ArrayList<>(Arrays.asList(sessionThree, sessionFour));

    private void loadData() {
        sessionRepository.add(userOneSessions);
        sessionRepository.add(userTwoSessions);
    }

    @After
    public void after() {
        sessionRepository.clear();
    }

    @Test
    public void testCreateEmptyRepository() {
        Assert.assertEquals(0, sessionRepository.count());
    }

    @Test
    public void testRemoveByUserId() {
        loadData();
        final int counter = sessionRepository.count();

        sessionRepository.removeByUserId(unknownUser.getId());
        Assert.assertEquals(counter, sessionRepository.count());

        sessionRepository.removeByUserId(userOne.getId());
        Assert.assertEquals(counter - 2, sessionRepository.count());
        for (@NotNull final Session session : userOneSessions) {
            Assert.assertFalse(sessionRepository.contains(session.getUserId()));
        }
    }

    @Test
    public void testFindByUserId() {
        loadData();

        @NotNull List<Session> sessions = sessionRepository.findByUserId(unknownUser.getId());
        Assert.assertEquals(0, sessions.size());

        sessions = sessionRepository.findByUserId(userOne.getId());
        Assert.assertNotNull(sessions);
        Assert.assertEquals(userOneSessions.hashCode(), sessions.hashCode());
        Assert.assertEquals(userOneSessions, sessions);
    }

}
