package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;

@Category(DataCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final User userOne = new User(
            "User1",
            HashUtil.salt("1111"),
            "1@1.ru"
    );

    @NotNull
    private final User userTwo = new User(
            "User2",
            HashUtil.salt("2222"),
            "2@2.ru"
    );

    @NotNull
    private final User unknownUser = new User(
            "Unknown",
            HashUtil.salt("3333"),
            "3@3.ru"
    );

    private void loadData() {
        userRepository.add(userOne, userTwo);
    }

    @After
    public void after() {
        userRepository.clear();
    }

    @Test
    public void testCreateEmptyRepository() {
        Assert.assertEquals(0, userRepository.count());
    }

    @Test
    public void testFindByLogin() {
        loadData();
        @Nullable User user = userRepository.findByLogin(unknownUser.getLogin());
        Assert.assertNull(user);

        user = userRepository.findByLogin(userOne.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(userOne.hashCode(), user.hashCode());
        Assert.assertEquals(userOne, user);
    }

    @Test
    public void testRemoveByLogin() {
        loadData();
        final int counter = userRepository.count();

        @Nullable User user = userRepository.removeByLogin(unknownUser.getLogin());
        Assert.assertNull(user);
        Assert.assertEquals(counter, userRepository.count());

        user = userRepository.removeByLogin(userOne.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(userOne, user);
        Assert.assertEquals(counter - 1, userRepository.count());
        user = userRepository.findByLogin(userOne.getLogin());
        Assert.assertNull(user);
    }

}
