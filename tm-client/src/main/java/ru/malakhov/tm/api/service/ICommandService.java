package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    @NotNull
    List<AbstractCommand> getCommandList();

}