package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findAll", propOrder = {
    "session"
})
public final class FindAll {

    protected Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

}