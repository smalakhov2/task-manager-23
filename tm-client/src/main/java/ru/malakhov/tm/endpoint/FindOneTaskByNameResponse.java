package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findOneTaskByNameResponse", propOrder = {
    "_return"
})
public final class FindOneTaskByNameResponse {

    @XmlElement(name = "return")
    protected Task _return;

    public Task getReturn() {
        return _return;
    }

    public void setReturn(Task value) {
        this._return = value;
    }

}