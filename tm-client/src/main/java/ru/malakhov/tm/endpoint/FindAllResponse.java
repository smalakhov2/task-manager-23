package ru.malakhov.tm.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findAllResponse", propOrder = {
    "_return"
})
public final class FindAllResponse {

    @XmlElement(name = "return")
    protected List<Project> _return;

    public List<Project> getReturn() {
        if (_return == null) {
            _return = new ArrayList<Project>();
        }
        return this._return;
    }

}