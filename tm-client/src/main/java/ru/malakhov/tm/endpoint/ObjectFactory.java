package ru.malakhov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public final class ObjectFactory {

    private final static QName _ChangeEmail_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeEmail");
    private final static QName _ChangeEmailResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeEmailResponse");
    private final static QName _ChangeFirstName_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeFirstName");
    private final static QName _ChangeFirstNameResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeFirstNameResponse");
    private final static QName _ChangeLastName_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeLastName");
    private final static QName _ChangeLastNameResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeLastNameResponse");
    private final static QName _ChangeLogin_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeLogin");
    private final static QName _ChangeLoginResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeLoginResponse");
    private final static QName _ChangeMiddleName_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeMiddleName");
    private final static QName _ChangeMiddleNameResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changeMiddleNameResponse");
    private final static QName _ChangePassword_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changePassword");
    private final static QName _ChangePasswordResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "changePasswordResponse");
    private final static QName _Create_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "create");
    private final static QName _CreateResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "createResponse");
    private final static QName _Profile_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "profile");
    private final static QName _ProfileResponse_QNAME = new QName("http://endpoint.tm.malakhov.ru/", "profileResponse");

    public ObjectFactory() {
    }

    public ChangeEmail createChangeEmail() {
        return new ChangeEmail();
    }

    public ChangeEmailResponse createChangeEmailResponse() {
        return new ChangeEmailResponse();
    }

    public ChangeFirstName createChangeFirstName() {
        return new ChangeFirstName();
    }

    public ChangeFirstNameResponse createChangeFirstNameResponse() {
        return new ChangeFirstNameResponse();
    }

    public ChangeLastName createChangeLastName() {
        return new ChangeLastName();
    }

    public ChangeLastNameResponse createChangeLastNameResponse() {
        return new ChangeLastNameResponse();
    }

    public ChangeLogin createChangeLogin() {
        return new ChangeLogin();
    }

    public ChangeLoginResponse createChangeLoginResponse() {
        return new ChangeLoginResponse();
    }

    public ChangeMiddleName createChangeMiddleName() {
        return new ChangeMiddleName();
    }

    public ChangeMiddleNameResponse createChangeMiddleNameResponse() {
        return new ChangeMiddleNameResponse();
    }

    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    public Create createCreate() {
        return new Create();
    }

    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    public Profile createProfile() {
        return new Profile();
    }

    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    public Session createSession() {
        return new Session();
    }

    public User createUser() {
        return new User();
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeEmail")
    public JAXBElement<ChangeEmail> createChangeEmail(ChangeEmail value) {
        return new JAXBElement<ChangeEmail>(_ChangeEmail_QNAME, ChangeEmail.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeEmailResponse")
    public JAXBElement<ChangeEmailResponse> createChangeEmailResponse(ChangeEmailResponse value) {
        return new JAXBElement<ChangeEmailResponse>(_ChangeEmailResponse_QNAME, ChangeEmailResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeFirstName")
    public JAXBElement<ChangeFirstName> createChangeFirstName(ChangeFirstName value) {
        return new JAXBElement<ChangeFirstName>(_ChangeFirstName_QNAME, ChangeFirstName.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeFirstNameResponse")
    public JAXBElement<ChangeFirstNameResponse> createChangeFirstNameResponse(ChangeFirstNameResponse value) {
        return new JAXBElement<ChangeFirstNameResponse>(_ChangeFirstNameResponse_QNAME, ChangeFirstNameResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeLastName")
    public JAXBElement<ChangeLastName> createChangeLastName(ChangeLastName value) {
        return new JAXBElement<ChangeLastName>(_ChangeLastName_QNAME, ChangeLastName.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeLastNameResponse")
    public JAXBElement<ChangeLastNameResponse> createChangeLastNameResponse(ChangeLastNameResponse value) {
        return new JAXBElement<ChangeLastNameResponse>(_ChangeLastNameResponse_QNAME, ChangeLastNameResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeLogin")
    public JAXBElement<ChangeLogin> createChangeLogin(ChangeLogin value) {
        return new JAXBElement<ChangeLogin>(_ChangeLogin_QNAME, ChangeLogin.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeLoginResponse")
    public JAXBElement<ChangeLoginResponse> createChangeLoginResponse(ChangeLoginResponse value) {
        return new JAXBElement<ChangeLoginResponse>(_ChangeLoginResponse_QNAME, ChangeLoginResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeMiddleName")
    public JAXBElement<ChangeMiddleName> createChangeMiddleName(ChangeMiddleName value) {
        return new JAXBElement<ChangeMiddleName>(_ChangeMiddleName_QNAME, ChangeMiddleName.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changeMiddleNameResponse")
    public JAXBElement<ChangeMiddleNameResponse> createChangeMiddleNameResponse(ChangeMiddleNameResponse value) {
        return new JAXBElement<ChangeMiddleNameResponse>(_ChangeMiddleNameResponse_QNAME, ChangeMiddleNameResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changePassword")
    public JAXBElement<ChangePassword> createChangePassword(ChangePassword value) {
        return new JAXBElement<ChangePassword>(_ChangePassword_QNAME, ChangePassword.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "changePasswordResponse")
    public JAXBElement<ChangePasswordResponse> createChangePasswordResponse(ChangePasswordResponse value) {
        return new JAXBElement<ChangePasswordResponse>(_ChangePasswordResponse_QNAME, ChangePasswordResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "create")
    public JAXBElement<Create> createCreate(Create value) {
        return new JAXBElement<Create>(_Create_QNAME, Create.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "createResponse")
    public JAXBElement<CreateResponse> createCreateResponse(CreateResponse value) {
        return new JAXBElement<CreateResponse>(_CreateResponse_QNAME, CreateResponse.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "profile")
    public JAXBElement<Profile> createProfile(Profile value) {
        return new JAXBElement<Profile>(_Profile_QNAME, Profile.class, null, value);
    }

    @XmlElementDecl(namespace = "http://endpoint.tm.malakhov.ru/", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

}