package ru.malakhov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


@WebService(targetNamespace = "http://endpoint.tm.malakhov.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/SessionEndpoint/closeRequest", output = "http://endpoint.tm.malakhov.ru/SessionEndpoint/closeResponse")
    @RequestWrapper(localName = "close", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.Close")
    @ResponseWrapper(localName = "closeResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CloseResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Result close(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/SessionEndpoint/openRequest", output = "http://endpoint.tm.malakhov.ru/SessionEndpoint/openResponse")
    @RequestWrapper(localName = "open", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.Open")
    @ResponseWrapper(localName = "openResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.OpenResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Session open(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/SessionEndpoint/closeAllRequest", output = "http://endpoint.tm.malakhov.ru/SessionEndpoint/closeAllResponse")
    @RequestWrapper(localName = "closeAll", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CloseAll")
    @ResponseWrapper(localName = "closeAllResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CloseAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Result closeAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

}