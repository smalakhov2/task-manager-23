package ru.malakhov.tm.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findAllTaskResponse", propOrder = {
    "_return"
})
public final class FindAllTaskResponse {

    @XmlElement(name = "return")
    protected List<Task> _return;

    public List<Task> getReturn() {
        if (_return == null) {
            _return = new ArrayList<Task>();
        }
        return this._return;
    }

}